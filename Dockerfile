FROM node:10-alpine

COPY --chown=node:node package.json /opt/app/
WORKDIR /opt/app
USER node

COPY --chown=node:node . /opt/app

EXPOSE 3000
CMD ["node", "app.js"]
