const express = require('express');
const router = new express.Router();


const utils = require('../helper/utils');
const account = require('../helper/account');

router.get('/', function(req, res) {
  res.render('registration');
});

router.post('/', async function(req, res) {
  if (req.body.password !== req.body.confirmpassword) {
    res.status(400).send({errorMessage: 'PASSWORDS_NOT_EQUALS'});
    return;
  }

  if (!utils.validateEmail(req.body.email)) {
    res.status(400).send({errorMessage: 'NOT EMAIL'});
    return;
  }

  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  try {
    account.createAccount(req.body.email, req.body.password, req.body.confirmpassword, req.body.fingerprint);
  } catch(e){
    res.status(400).send({errorMessage: e.message});
    return;
  }
  res.status(200).send();
});

module.exports = router;
