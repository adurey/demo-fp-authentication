const express = require('express');
const account = require('../helper/account')
const router = new express.Router();

router.get('/', function(req, res) {
    res.render('home');
});

module.exports = router;
