const express = require('express');
const router = new express.Router();

const account = require('../helper/account');

router.get('/', function(req, res) {
  res.render('authentication');
});

router.post('/', async function(req, res) {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  let token;
  try {
    token = account.authenticate(req.body.email, req.body.password, req.body.fingerprint, ip, req.headers, req.body.token);
  } catch(e){
    res.status(400).send({errorMessage: e.message});
    return;
  }
  res.status(200).send({'token': token});
});


module.exports = router;
