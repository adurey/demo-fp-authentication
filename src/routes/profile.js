const express = require('express');
const router = new express.Router();

const account = require('../helper/account');
const fingerprint = require('../helper/fingerprint');


router.get('/', function(req, res) {
    res.render('profile', {email: res.locals.email});
});

router.get('/fingerprints', function(req, res) {
    const fingerprints = account.getFingerprints(res.locals.email);
    const fingerprintsAddedTime = Object.keys(fingerprints);
    fingerprintsAddedTime.forEach(function(fingerprintAddedTime){
        const f = fingerprints[fingerprintAddedTime];
        const uaparsed = fingerprint.parseUserAgent(f);
        let os = uaparsed.os.name;
        if(uaparsed.os.version !== "" && uaparsed.os.version !== undefined) {
            os += " " + uaparsed.os.version;
        }

        let browser = uaparsed.browser.name;
        if(uaparsed.browser.major !== "" && uaparsed.browser.major !== undefined) {
            browser += " " + uaparsed.browser.major;
        }
        f.os = os;
        f.browser = browser;
        f.date = new Date(parseInt(fingerprintAddedTime)).toUTCString()

        fingerprints[fingerprintAddedTime] = f;
    });

    res.render('fingerprints', {fingerprints: fingerprints});
});

router.get('/connexionattempts', function(req, res) {
    const connexionAttempts = account.getConnexionAttempts(res.locals.email);
    const connexionAttemptsTime = Object.keys(connexionAttempts);
    connexionAttemptsTime.forEach(function(connexionAttemptTime){
        const connexionAttempt = connexionAttempts[connexionAttemptTime];
        const uaparsed = fingerprint.parseUserAgent(connexionAttempt.fingerprint);
        let os = uaparsed.os.name;
        if(uaparsed.os.version !== "" && uaparsed.os.version !== undefined) {
            os += " " + uaparsed.os.version;
        }

        let browser = uaparsed.browser.name;
        if(uaparsed.browser.major !== "" && uaparsed.browser.major !== undefined) {
            browser += " " + uaparsed.browser.major;
        }
        connexionAttempt.os = os;
        connexionAttempt.browser = browser;
        connexionAttempt.date = new Date(parseInt(connexionAttemptTime)).toUTCString()

        connexionAttempts[connexionAttemptTime] = connexionAttempt;
    });

    res.render('connexionAttempts', {connexionAttempts: connexionAttempts});
});

router.get('/admin', function(req, res) {
    const parameters = account.getParameters(res.locals.email);
    res.render('parameters', {parameters: parameters})
});


router.post('/admin', function(req, res) {
    account.saveParameters(res.locals.email, req.body);

    res.status(200).send();
});


module.exports = router;
