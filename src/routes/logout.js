const express = require('express');
const router = new express.Router();

const session = require('../helper/account');

router.get('/', function(req, res) {
  // .authentication_key
  const cookies = req.headers.cookie.replace(/\s/g, '').split(';');
  for (let i in cookies) {
    const cookie = cookies[i];
    const elements = cookie.split('=');
    if (elements.length >= 1 && elements[0] === 'Session-Id') {
      session.deleteSession(elements[1]);
    }
  }

  res.redirect('/authentication');
});

module.exports = router;
