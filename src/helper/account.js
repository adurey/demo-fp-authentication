const ipCheck = require('ip-range-check');
const nodemailer = require('nodemailer');
const fs = require('fs')

const f = require('../helper/fingerprint');
const configuration = require('../helper/configuration.js');

let accounts, activeSessions;

const ADD_FINGERPRINT_CHECK_ALLOW_X_MINUTES = "ALLOW_X_MINUTES";
const ADD_FINGERPRINT_CHECK_EMAIL = "EMAIL";
const ADD_FINGERPRINT_CHECK_NETWORK = "NETWORK";

const init = function() {
    accounts = {};
    activeSessions = {};

};

const authenticate = function(email, password, fingerprint, ipAddress, headers, token) {

    if(!(email in accounts)) {
        throw new Error("Account does not exist")
    }

    const account = accounts[email];
    if(account.password !== password) {
        throw new Error("Password is incorrect")
    }

    if(typeof fingerprint.isBot === 'string') {
        fingerprint.isBot = fingerprint.isBot === 'true';
    }

    if(fingerprint.isBot){
        addConnexionAttempts(email, false, fingerprint);
        throw new Error("You have been detected as a bot. Permission denied");
    }

    if(f.compareAllAuthentication(account.fingerprints, fingerprint)){
        addConnexionAttempts(email, true, fingerprint);
        return createSession(email, headers);
    }

    // Authentication via timer method (new fingerprint)
    if(account.parameters.addFingerprintCheckType === ADD_FINGERPRINT_CHECK_ALLOW_X_MINUTES && new Date().getTime() < account.parameters.addFingerprintTimer) {
        resetAddFingerprintTime(email);
        account.fingerprints[new Date().getTime()] = fingerprint;
        addConnexionAttempts(email, true, fingerprint);
        return createSession(email, headers);
    }

    // Authentication via network method (new fingerprint)
    if(account.parameters.addFingerprintCheckType === ADD_FINGERPRINT_CHECK_NETWORK) {
        if(ipCheck(ipAddress, account.parameters.formattedIPAddress)){
            account.fingerprints[new Date().getTime()] = fingerprint;
            addConnexionAttempts(email, true, fingerprint);
            return createSession(email, headers);
        }
    }
    // Authentication via email method (new fingerprint)
    if(token !== "" && token !== undefined) {
        const index = account.parameters.tokens.indexOf(token);
        if (index >= 0) {
            account.parameters.tokens.splice(index, 1);
            account.fingerprints[new Date().getTime()] = fingerprint;
            addConnexionAttempts(email, true, fingerprint);
            return createSession(email, headers);
        }
    }

    addConnexionAttempts(email, false, fingerprint);

    throw new Error("Your fingerprint doesn't match the fingerprints registered for this account");
};

const createAccount = function(email, password, confirmpassword, fingerprint) {
    if(!email.length) {
        throw new Error("Email empty");
    }

    if(!password.length) {
        throw new Error("Password empty");
    }

    if(password !== confirmpassword) {
        throw new Error("Email empty");
    }

    if(email in accounts) {
        throw new Error("Account already exists");
    }

    if(typeof fingerprint.isBot === 'string') {
        fingerprint.isBot = fingerprint.isBot === 'true';
    }

    const account = {
        password: password,
        confirmed: false,
        master: fingerprint,
        fingerprints: {},
        connexionAttempts: {},
        tokens: {},
        parameters: {
            sessionHijackingProtection: false,
            addFingerprintCheckType: undefined,
            email: "",
            ipAddress: "",
            formattedIPAddress: [],
            tokens: []
        }
    };

    account.fingerprints[new Date().getTime()] = fingerprint;

    accounts[email] = account;

};

const addConnexionAttempts = function(email, success, fingerprint) {
    const connexionAttempt = {
        success: success,
        fingerprint: fingerprint
    };
    const time = new Date().getTime();
    accounts[email].connexionAttempts[time] = connexionAttempt;
};

const getConnexionAttempts = function(email) {
    return accounts[email].connexionAttempts
};

const getFingerprints = function(email) {
    return accounts[email].fingerprints
};

const removeFingerprint = function(email, id) {
    if(!(email in accounts)) {
        throw "Account does not exist"
    }
    accounts[email].fingerprints.delete(id)
};

const saveParameters = function(email, body) {
    if(!(email in accounts)) {
        throw "Account does not exist"
    }

    const account = accounts[email];

    account.parameters.sessionHijackingProtection = !!body.sessionHijackingProtection;
    account.parameters.addFingerprintCheckType = body.addFingerprintCheck;
    account.parameters.email = body.email;
    account.parameters.ipAddress = body.ipAddress;
    account.parameters.formattedIPAddress = [];

    ipAddresses = account.parameters.ipAddress.split(',');
    for(let i in ipAddresses) {
        let ipAddress = ipAddresses[i].trim();
        account.parameters.formattedIPAddress.push(ipAddress);
    }

    if(body.timer) {
        startTimer(email);
    }

    if(body.sendEmail) {
        sendEmail(email, account.parameters.email);
    }

    accounts[email] = account
};

const getParameters = function(email) {
    if(!(email in accounts)) {
        throw "Account does not exist"
    }

    const parameters = accounts[email].parameters;
    if(parameters.addFingerprintCheckType === undefined){
        parameters.addFingerprintCheckType = ADD_FINGERPRINT_CHECK_ALLOW_X_MINUTES
    }

    if(parameters.addFingerprintNetworkAddresses === undefined){
        parameters.addFingerprintNetworkAddresses = "";
    }

    if(parameters.sessionHijackingProtection === undefined){
        parameters.sessionHijackingProtection = false;
    }

    return parameters
};

const startTimer = function(email) {
    if(!email in accounts) {
        throw "Account does not exist"
    }

    const account = accounts[email];
    const d = new Date();
    d.setMinutes(d.getMinutes() + 15);
    account.parameters.addFingerprintTimer = d.getTime();
};

const sendEmail = function(emailID, emailToSend) {
    const conf = configuration.getConfiguration();
    fs.readFile('./public/email-template/add-device.html', 'utf8', (err, data) => {
        if (err) throw err;

        const url = conf.web.url + ":" + conf.web.port.toString() + "/authentication?token=" + generateEmailToken(emailID);
        data = data.replace(/{{ .Email }}/g, emailToSend);
        data = data.replace(/{{ .AddNewDeviceURL }}/g, url);

        const transporter = nodemailer.createTransport({
            service: conf.smtp.type,
            auth: {
                user: conf.smtp.username,
                pass: conf.smtp.password
            }
        });

        const mailOptions = {
            from: conf.smtp.email,
            to: emailToSend,
            subject: '[FP-Secure-Demo] Request for adding a new device',
            html: data
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            }
        });
    });
};
///////////////////////////
//    SESSION
///////////////////////////

const createSession = function(email, headers) {
    const token = createToken();
    activeSessions[token] = {
        email: email,
        headers: headers,
    };
    return token;
};

const checkSession = function(token, headers) {
    if (!(token in activeSessions)) {
        return "";
    }
    const session = activeSessions[token];
    const account = accounts[session.email];
    if('sessionHijackingProtection' in account.parameters && account.parameters.sessionHijackingProtection) {
        if (session.headers['accept-language'] !== headers['accept-language'] ||
            session.headers['user-agent'] !== headers['user-agent']) {
            return "";
        }
    }
    return session.email
};

const deleteSession = function(token) {
    if (!(token in activeSessions)) {
        return;
    }
    delete activeSessions[token];
};

const createToken = function() {
    let token = '';
    while (token === '' || token in activeSessions) {
        token = '' + Math.random().toString(36).substr(2, 17);
    }

    return token;
};

const invalidateSession = function(id) {
    activeSessions.delete(id);
};

const generateEmailToken = function(email) {
    const account = accounts[email];
    let token = '';
    while (token === '' || account.parameters.tokens.indexOf(token) >= 0) {
        token = '' + Math.random().toString(36).substr(2, 17);
    }

    account.parameters.tokens.push(token);

    return token
};

const resetAddFingerprintTime = function(email) {
    if (!(email in accounts)) {
        return false;
    }

    const account = accounts[email];
    account.parameters.addFingerprintTimer = new Date().getTime();
};



module.exports = {
    'init': init,
    'createAccount': createAccount,
    'authenticate': authenticate,
    'getConnexionAttempts': getConnexionAttempts,
    'getParameters': getParameters,
    'getFingerprints': getFingerprints,
    'removeFingerprint': removeFingerprint,
    'saveParameters': saveParameters,
    'startTime': startTimer,
    'createSession': createSession,
    'checkSession': checkSession,
    'deleteSession': deleteSession,
    'createToken': createToken,
    'invalidateSession': invalidateSession,
    'resetAddFingerprintTime': resetAddFingerprintTime,
    'sendEmail': sendEmail,
};
