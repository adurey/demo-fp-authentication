let configuration = undefined;

const setConfiguration = function(newConfiguration) {
    if(configuration !== undefined) {
        throw new Error("Configuration already set");
    }

    configuration = newConfiguration;
};

const getConfiguration = function() {
    if(configuration === undefined) {
        throw new Error("Configuration not set");
    }
    return configuration
};

module.exports = {
    'setConfiguration': setConfiguration,
    'getConfiguration': getConfiguration
}