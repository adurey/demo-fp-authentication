const UAParser = require('ua-parser-js');


const compareAllAuthentication = function(fingerprintsAccount, fingerprintToCompare) {
    const keys = Object.keys(fingerprintsAccount);
    for(let i in keys) {
        const key = keys[i];
        const fingerprintAccount = fingerprintsAccount[key];
        if(compareAuthentication(fingerprintAccount, fingerprintToCompare)){
            return true;
        }
    }
    return false;
};

const compareAuthentication = function(fingerprintAccount, fingerprintToCompare) {
    const keysNotChecked = ["language", "timezone", "connexion-attempt", "date", "os", "browser"];
    const keys = Object.keys(fingerprintAccount);
    for(let i in keys) {
        const key = keys[i];
        if (keysNotChecked.indexOf(key) === -1 && fingerprintAccount[key] !== fingerprintToCompare[key]) {
            return false;
        }
    }
    return true;
};

const parseUserAgent = function(fingerprint) {
    const uaParser = new UAParser(fingerprint.userAgent);
    return uaParser.getResult()
};

module.exports = {
    'compareAllAuthentication': compareAllAuthentication,
    'compareAuthentication': compareAuthentication,
    'parseUserAgent': parseUserAgent,
};
