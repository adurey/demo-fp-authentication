# FP-Authenticator demo v2.3

This demo aims to show how fingerprint can enhance web authentication

## How to run

`docker-compose up`

## How to use

Go to `localhost:8091` (or with whatever port you put)

You will have 2 choices :
  - create an account. After creating an account, you will be redirected on the connexion page
  - connect. After connecting, you will be redirected on your profile page. There is nothing on it for the moment

Please note if you're authenticated, going on the connexion page will redirect you on the profile page. Similarly, if you're not authenticated, going on the profile page will redirect you on the connexion page. The cookie set expires after 10 minutes.

## The fingerprint
Contains attributes, such as the following elements (not exhaustive) :
- userAgent
- appCodeName
- platform
- timezone
- languages
- webGLRenderer
- webGLVendor
- canvas

