(async () => {
  const express = require('express');
  const cookieParser = require('cookie-parser');
  const path = require('path');
  const configuration = require('./src/helper/configuration');

  const configFile = require('./conf/conf.json');
  configuration.setConfiguration(configFile);

  const account = require('./src/helper/account')
  account.init();

  const app = express();
  const homeRouter = require('./src/routes/home');
  const registrationRouter = require('./src/routes/registration');
  const authenticationRouter = require('./src/routes/authentication');
  const logoutRouter = require('./src/routes/logout');
  const profileRouter = require('./src/routes/profile');

  app.set('view engine', 'pug');

  app.use(express.json({limit: '10mb', extended: true}));
  app.use(express.urlencoded({ extended: true }));
  app.use(cookieParser());

  // Static files
  // view engine setup
  app.use(express.static(path.join(__dirname, './public')));


  app.use(function(req,res,next){
    const allowedPathNotAuthenticated = ['/', '/favicon.ico', '/authentication', '/registration']
    if (!(allowedPathNotAuthenticated.includes(req.path))) {
      // If the path is not part of the authorized path without session, then we check the session
      const email = account.checkSession(req.cookies["Session-Id"], req.headers);
      if(email === "") {
        res.redirect('/authentication');
        return
      }
      res.locals.email = email;
    }
    next();
  });

  app.use('/', homeRouter);
  app.use('/profile', profileRouter);
  app.use('/registration', registrationRouter);
  app.use('/authentication', authenticationRouter);
  app.use('/logout', logoutRouter);
  
  app.listen(3000);

  module.exports = app;
})().catch(e => {
  console.log(e)
});
