let fingerprint;(style="display: block;")

$.when(GetFingerprint()).done(function(fp){
    fingerprint = fp
});

function connect(){

    const url = new URL(window.location);
    const token = url.searchParams.get("token");

    $.ajax({
        type: "POST",
        url:  "/authentication",
        data: {"email": $("#email").val(), "password": $("#password").val(), "fingerprint": fingerprint, "token": token},
        success: function(data){
            CreateCookie("Session-Id", data["token"])

            window.location.replace("/profile")
        },
        error: function(xhr, status, error) {
            let err = eval("(" + xhr.responseText + ")");

            $("#connexionComment").attr('class', "alert alert-danger")
            $("#connexionComment").html(err.errorMessage)
        }
    });
}

$('form').submit(function(){
    connect()
    return false
});
