$(function() {
    $("#navbar").show();

    const trs = $("tbody tr");
    trs.each(function(i){
        const tr = trs[i];
        const attribute = tr.childNodes[0].innerText;
        const tdValue = tr.childNodes[1];
        if(attribute === "canvas"){
            const canvasElement = document.createElement('canvas');
            canvasElement.height = 60;
            canvasElement.width = 400;
            const canvasData = tdValue.innerText;

            let canContext = canvasElement.getContext("2d");
            let canImage = new Image();
            canImage.src = canvasData;

            canImage.onload = function(){
                canContext.drawImage(canImage, 0, 0);
            };

            tdValue.innerText = "";
            tdValue.appendChild(canvasElement);

        } else if (attribute === "font") {
            tdValue.innerText = FormatIntoList(tdValue.innerText, ', ', 10);
        } else if (attribute === "audioFormats" || attribute === "videoFormats") {
            tdValue.innerHTML = FormatIntoList(tdValue.innerText, '<br/>', 10);
        } else if(attribute === "webGLParameters") {
            const jsonParsed = JSON.parse(tdValue.innerText);
            const extensionsParsed = JSON.parse(jsonParsed.extensions);
            tdValue.innerHTML = extensionsParsed.length + " extensions<br/>";
            tdValue.innerHTML += Object.keys(jsonParsed.general).length + " general parameters<br/>";
            tdValue.innerHTML += Object.keys(jsonParsed.shaderPrecision).length + " shader precisions";
        } else {
            // Else, do nothing
        }
    });

    $("body").on('click', '.fa-plus', function(event){
        $(this).closest("div.card").find("div.card-body").show()
        $(this).removeClass('fa-plus')
        $(this).addClass('fa-minus')
    });

    $("body").on('click', '.fa-minus', function(event){
        $(this).closest("div.card").find("div.card-body").hide()
        $(this).removeClass('fa-minus')
        $(this).addClass('fa-plus')

    })
});