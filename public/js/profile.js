$(function(){
    $("#navbar").show();

    const lang = navigator.language
    let src;

    if(lang === "fr" || lang.startsWith("fr")){
        src = "/img/schema-FIC-fr.png"
    } else {
        src = "/img/schema-FIC-en.png"
    }

    $("#schema-img").attr("src", src)

    $.when(GetFingerprint()).done(function(fingerprint) {
        DisplayFingerprintIntoTBody(document.getElementById("tbody-element"), fingerprint)
    });
})