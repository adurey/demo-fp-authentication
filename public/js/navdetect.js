

function navdetect(){
    var nav = "Autre";

    var ua = navigator.userAgent;

    var x = ua.indexOf("Firefox");
    if (x!=-1) {
        nav = "Firefox";
    }
    else {
        x = ua.indexOf("Chromium");
        if (x!=-1) {
            nav = "Chromium";
        }
        else {
            x = ua.indexOf("Chrome");
            if (x!=-1) {
                nav = "Chrome";
            }
        }
    }
    //nav = "Autre";
    return nav;
}

var mynav = navdetect();
var mynavlow = mynav.toLowerCase();
//Set Logo
var logo_path = "/img/"+mynavlow+"_logo.png";
$("#navlogo").attr("src", logo_path);

//Set Nav name
$("#navname").text(mynav);

//Set CSS
var css_path = "/css/nav"+mynavlow+".css";
$("#cssfile").attr('href', css_path);
