let fingerprint;
$.when(GetFingerprint()).done(function(fp){
    fingerprint = fp
});

function createAccount(){
    console.log($("#email").val());
    $.ajax({
        type: "POST",
        url:  "/registration",
        data: {"email": $("#email").val(), "password": $("#password").val(), "confirmpassword": $("#confirmPassword").val(), "fingerprint": fingerprint},
        success: function(data, status){
            window.location.replace("/authentication")
        } ,
        error: function(xhr, status, error) {
            const err = eval("(" + xhr.responseText + ")");

            $("#creatingAccountComment").attr('class', "alert alert-danger")
            $("#creatingAccountComment").html(error)
        }
    });
};

$('form').submit(function(){
    createAccount();
    return false
});