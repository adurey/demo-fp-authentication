/*
  Consider using these polyfills to broaden browser support:
    — https://www.npmjs.com/package/classlist-polyfill
    — https://www.npmjs.com/package/nodelist-foreach-polyfill
*/

var container = document.querySelector('.nav__tabs');
var primary = container.querySelector('.nav__primary');
var primaryItems = container.querySelectorAll('.nav__primary > li:not(.-more)');
container.classList.add('--jsfied');

// insert "more" button and duplicate the list

primary.insertAdjacentHTML('beforeEnd', `
  <li class="-more nav-item">
    <button type="button" aria-haspopup="true" aria-expanded="false" class="btn btn-primary">
      More <span>&darr;</span>
    </button>
    <ul class="nav__secondary nav bg-primary">
      ${primary.innerHTML}
    </ul>
  </li>
`);

var secondary = container.querySelector('.nav__secondary');
$('.nav__secondary li').addClass('nav-item');
$('.nav__secondary li a').addClass('nav-link');


var secondaryItems = secondary.querySelectorAll('li');
var allItems = container.querySelectorAll('li');
var moreLi = primary.querySelector('.-more');
var moreBtn = moreLi.querySelector('button');
var amIUnique = document.querySelector('.navbar-header');

moreBtn.addEventListener('click', (e) => {
  e.preventDefault();
  container.classList.toggle('--show-secondary');
  moreBtn.setAttribute('aria-expanded', container.classList.contains('--show-secondary'));
});

// adapt tabs

var doAdapt = () => {
  // reveal all items for the calculation
  allItems.forEach((item) => {
    item.classList.remove('--hidden');
  });

  // hide items that won't fit in the Primary
  var stopWidth = amIUnique.offsetWidth + moreBtn.offsetWidth;
  var hiddenItems = [];
  var navbarWidth = document.querySelector('nav.navbar').offsetWidth
  var hideEverythingElse = false;
  primaryItems.forEach((item, i) => {
    if(!hideEverythingElse && navbarWidth >= (stopWidth + item.offsetWidth + 10)) {
      stopWidth += item.offsetWidth;
    } else {
      item.classList.add('--hidden');
      hiddenItems.push(i);
      hideEverythingElse = true;
    }
  });

  // toggle the visibility of More button and items in Secondary
  if(!hiddenItems.length) {
    moreLi.classList.add('--hidden');
    container.classList.remove('--show-secondary');
    moreBtn.setAttribute('aria-expanded', false);
  }
  else {
    secondaryItems.forEach((item, i) => {
      if(!hiddenItems.includes(i)) {
        item.classList.add('--hidden');
      }
    })
  }
};

doAdapt(); // adapt immediately on load
window.addEventListener('resize', doAdapt); // adapt on window resize

// hide Secondary on the outside click

document.addEventListener('click', (e) => {
  let el = e.target;
  while(el) {
    if(el === secondary || el === moreBtn) {
      return;
    }
    el = el.parentNode;
  }
  container.classList.remove('--show-secondary');
  moreBtn.setAttribute('aria-expanded', false);
});