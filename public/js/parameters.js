$(function(){
    $("#navbar").show();

    let checked = "";
    let hijacking = "";
    let email = "";
    let ipAddress = "";

    $("#formSessionHijacking").change(function() {
        hijacking = $("#formSessionHijacking")[0].checked;
        saveChanges(hijacking, checked, false, email, ipAddress, false)
    });


    $("#firstConnexionButton").click(function(){
        console.log('firstConnexionButton clicked');
        checked = "ALLOW_X_MINUTES"
        saveChanges(hijacking, checked, true, email, ipAddress, false);

    });


    $('#buttonIPAddresses').click(function(){
        console.log('buttonIPAddresses clicked');

        checked = "NETWORK";
        ipAddress = $("input[name=ipAddresses]").val()
        saveChanges(hijacking, checked, false, email, ipAddress, false);
    });


    $('#buttonEmail').click(function(){
        console.log('email clicked');

        email = $("#email").val()
        checked = "EMAIL";

        saveChanges(hijacking, checked, false, email, ipAddress, true);
    });

    let saveChanges = function(hijacking, checked_option, timer, email, ipAddress, sendEmail){
        $("#saveComment").hide();

        const data = {
            "sessionHijackingProtection": hijacking,
            "addFingerprintCheck": checked_option,
            "email": email,
            "ipAddress": ipAddress
        };

        if(data.addFingerprintCheck === "ALLOW_X_MINUTES" && timer) {
            data.timer = true;
        }

        if(data.addFingerprintCheck === "EMAIL" && sendEmail) {
            data.sendEmail = true;
        }

        $.ajax({
            type: "POST",
            url:  "/profile/admin",
            data: data,
            success: function(data){ },
            error: function(){ }
        });
    }
});